export default {
  SET_USER: (state,user) => {
    localStorage.setItem('yeojongki-blog-user', user.name)
    state.user = user
  },
  ISLOGIN: (state) => {
  	state.isLogin = !state.isLogin
  },
  ISADMIN:(state) => {
    state.isAdmin = !state.isAdmin
  },
  SET_ARTICLE: (state,article) => {
  	state.article = article
  },
  SET_ARTICLELIST: (state,articleList) => {
		state.articleList = articleList
  },
  SET_CURRENT_ARTICLE: (state,Article) => {
    state.currentArticle = Article
  },
  CHANGE_ARTICLE_STATE:(state) => {
    state.currentArticle.state = -1
  },
  CLEAN_CURRENTARTICLE:(state) => {
    state.currentArticle = ''
  },
  SET_ALL_PAGE:(state,allPage) => {
    state.allPage = allPage;
  },
  SET_CUR_PAGE:(state,curPage) => {
    state.curPage = curPage;
  }
}