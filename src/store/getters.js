export default {
	user:state => state.user,
	allPage:state => state.allPage,
	curPage:state => state.curPage,
	currentArticle:state => state.currentArticle,
	articleLists:state => state.articleList
}