import Vue from 'vue'
import Vuex from 'vuex'
import actions from './actions'
import mutations from './mutations'
import getters from './getters'

const store = new Vuex.Store({
  state:{
    user: { 
      name: '', 
      password: '' 
    },
    isLogin:false,
    isAdmin:false,
    articleList: [],
    currentArticle: {
      date:'',
      title:'',
      content:'',
      description:'',
      state:'1',
      id:''
    },
    allPage:null,
    curPage:1
  },
  mutations,
  getters,
  actions
})
export default store
