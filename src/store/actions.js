import Axios from 'axios'
export default {
	signUp ({commit,state},user){
		return Axios.post('/api/signUp',user).then((res) => {
      return new Promise((resolve, reject) => {
        resolve(res)
      });
		})
	},
	login ({commit,state},user){
		return Axios.post('/api/login',user).then((res) => {
      return new Promise((resolve, reject) => {
        resolve(res);
        if(res.data.state == 3){
          commit('SET_USER',user)
        }
      });
		})
	},
	getArticleList({commit,state},query) {
		return Axios.post('api/getArticleList',query).then((res) => {
      return new Promise((resolve, reject) => {
        commit('SET_ARTICLELIST',res.data.doc)
        commit('SET_CURRENT_ARTICLE',res.data[0])
        commit('SET_ALL_PAGE',res.data.allPage)
        commit('SET_CUR_PAGE',query.page)
        resolve(res)
      });
		})
	},
	publishArticle({commit,state},article) {
		return Axios.post('api/publishArticle',article).then((res) => {
      return new Promise((resolve, reject) => {
        commit('SET_CURRENT_ARTICLE', article)
        commit('CHANGE_ARTICLE_STATE')
        commit('SET_ARTICLELIST', [])
        resolve(res)
      });
		})
	},
	saveArticle({commit,state},article) {
		return Axios.post('api/saveArticle',article).then((res) => {
      return new Promise((resolve, reject) => {
      	commit('SET_CURRENT_ARTICLE',article)
        resolve(res)
      });
		})
	},
	deleteArticle({commit,state},id) {
		return Axios.post('api/deleteArticle',id).then(() => {
      return new Promise((resolve, reject) => {
        commit('SET_ARTICLELIST', [])
        resolve()
      });
		})
	},
}
