import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

const signUp = resolve => require(['components/back/signUp'],resolve)
const login = resolve => require(['components/back/login'],resolve)
const index = resolve => require(['components/front/index'],resolve)
const admin = resolve => require(['components/back/admin'],resolve)
const article = resolve => require(['components/front/article'],resolve)

const router = new Router({
	mode:'history',
  routes: [
    {path:'/',component:index},
    {path: '/signUp',component: signUp},
    {path: '/login',component: login},
    {path:'/admin',component:admin},
    {path: '/article',name:'article',component:article},
    {path:'*',redirect:'/'}
  ],
})
export default router