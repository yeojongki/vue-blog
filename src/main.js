// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Axios from 'Axios'
import store from './store'
import 'babel-polyfill'
import { Message, MessageBox } from 'element-ui'

Vue.config.devtools = false
Vue.config.productionTip = false
Vue.prototype.$message = Message
Vue.prototype.$msgbox = MessageBox
Vue.prototype.$confirm = MessageBox.confirm;
Vue.prototype.$http = Axios

Vue.filter('formatTime',value =>{
	return (value+'').substring(0,10)
})
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
