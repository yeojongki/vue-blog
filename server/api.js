const db = require('./db')
const express = require('express')
const router = express.Router()
const md5 = require('md5')

/************** 创建(create) 读取(get) 更新(update) 删除(delete) **************/
// 注册接口
router.post('/api/signUp', (req, res) => {
  // 这里的req.body能够使用就在index.js中引入了const bodyParser = require('body-parser')
  let name = req.body.name
  let password = req.body.password
  let password_Re = req.body.password_Re
  db.User.findOne({ name: name }, (err, data) => {
    if (data) {
      res.send({ state: 0, msg: '帐号已存在' })
    } else if (name == '' || password == '' || password_Re == '')
      res.send({ state: 1, msg: '用户名或密码不能为空' })
    else if (password.length < 6) res.send({ state: 2, msg: '密码必须大于6位' })
    else if (password !== password_Re)
      res.send({ state: 3, msg: '两次输入的密码不一致' })
    else {
      let newUser = new db.User({
        name: name,
        password: password,
        password_Re: password_Re
      })
      newUser.save((err, data) => {
        res.send({
          state: 4,
          msg: '注册成功,即将进入登录界面...'
        })
      })
    }
  })
})

// 登录接口
router.post('/api/login', (req, res) => {
  //数据库中查找数据
  let name = req.body.name
  let password = req.body.password
  db.User.findOne({ name: name }, (err, data) => {
    switch (true) {
      case err:
        res.send(err)
        break
      case name == '' || password == '':
        res.send({ state: 0, msg: '用户名或密码不能为空' })
        break
      case data == null:
        res.send({ state: 1, msg: '用户名不存在' })
        break
      case password !== data.password:
        res.send({ state: 2, msg: '密码错误' })
        break
      case password == data.password:
        res.send({ state: 3, msg: '登陆成功,即将进入主页...' })
        break
      default:
        res.send({ state: 4, msg: '未知错误' })
    }
  })
})

//保存文章
router.post('/api/saveArticle', (req, res) => {
  let article = {
    title: req.body.title,
    content: req.body.content,
    description: req.body.description,
    state: req.body.state
  }
  if (req.body.title == '' || req.body.content == '') {
    res.send({ msg: '标题或内容不能为空', type: 'warning' })
    res.status(200).end()
  } else {
    db.Article.findOne({ _id: req.body.id }, (err, doc) => {
      if (err) {
        console.log(err)
      } else if (doc) {
        article.edit_time = req.body.date
        db.Article.findByIdAndUpdate(doc.id, article, err => {
          if (err) {
            console.log(err)
          } else {
            res.send({ msg: '文章更新成功', type: 'success' })
          }
        })
      } else {
        article.date = req.body.date
        new db.Article(article).save()
        res.send({ msg: '文章保存成功', type: 'success' })
      }
    })
  }
})

//发布文章
router.post('/api/publishArticle', (req, res) => {
  const id = req.body._id
  db.Article.findByIdAndUpdate(id, {state: -1}, (err, doc) => {
    if (err) {
      res.end({ type: 'error', msg: '发布失败' + err })
      return
    } else {
      res.send({ type: 'success', msg: '发布成功' })
    }
  })
  // db.Article.findByIdAndUpdate(req.body._id,article,(err) => {
  //   if(err) {
  //     console.log(err)
  //   }else {
  //     res.status(200).send('succeed in updating')
  //   }
  // })
})

//获取所有文章
router.post('/api/getArticleList', (req, res) => {
  const limit = req.body.limit
  let page = req.body.page
  let allNum
  let allPage
  let skip = 0
  if (page !== 0) {
    skip = limit * (page - 1)
  }
  db.Article.find(null, function(err, doc) {
    allNum = doc.length
    db.Article.find()
      .limit(limit)
      .skip(skip)
      .exec(function(err, doc) {
        allPage = Math.ceil(allNum / limit)
        if (err) {
          console.log(err)
        } else if (doc) {
          res.send({ doc: doc, allPage: allPage })
        }
      })
  })
})

//文章详情
router.get('/api/getArticle', (req, res) => {
  const id = req.query[0]
  db.Article.findOne({ _id: id }, (err, doc) => {
    if (err) {
      console.log(err)
    } else if (doc) {
      res.send(doc)
    }
  })
})

//删除文章
router.post('/api/deleteArticle', (req, res) => {
  db.Article.findByIdAndRemove(req.body.id, (err, doc) => {
    if (err) {
      console.log(err)
    } else if (doc) {
      res.send(doc)
      res.status(200).end()
    }
  })
})
module.exports = router
