// Schema、Model、Entity或者Documents的关系请牢记，Schema生成Model，Model创造Entity，Model和Entity都可对数据库操作造成影响，但Model比Entity更具操作性。
const mongoose = require('mongoose');
// 连接数据库 如果不自己创建 默认test数据库会自动生成
// mongoose.connect('mongodb://localhost/test');
mongoose.connect('mongodb://yeojongki:kk597.@ds029107.mlab.com:29107/yeojongki_blog');

// 为这次连接绑定事件
const db = mongoose.connection;
db.once('error', () => console.log('Mongo connection error'));
db.once('open', () => console.log('Mongo connection successed'));
/************** 定义模式userSchema **************/
const userSchema = mongoose.Schema({
	name: String,
	password: String,
	password_Re:String
})
const articleSchema = mongoose.Schema({
	date: Date,
  title: String,
  content: String,
  description: String,
	state: Number,
	edit_time:Date
})
/************** 定义模型Model **************/
const Models = {
	User: mongoose.model('User', userSchema),
	Article: mongoose.model('Article', articleSchema)
}

module.exports = Models;